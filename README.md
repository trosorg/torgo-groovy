![](https://tros.org/torgo/tros-images/torgo-orange-and-green.svg)  

# Logo Interpreter in Groovy

Torgo is a Logo interpreter written in Java. It uses [ANTLR](http://www.antlr.org/) as a language parser and for lexical analysis. The parsed scripts are then walked to interpret the commands.

Torgo is built using Java8.

Some goals for Torgo are:

- to allow people to learn to program
- to show how machines execute programs
- to allow modifications and add to languages to change behaviors

This project was inspired by [Tortue](http://tortue.sourceforge.net/).

## Languages

Torgo currently supports logo.

## Features

- Implementation in Groovy

## Download and Install

## Compile

```sh
git clone https://github.com/ZenHarbinger/torgo.git
cd torgo
mvn clean package
```
